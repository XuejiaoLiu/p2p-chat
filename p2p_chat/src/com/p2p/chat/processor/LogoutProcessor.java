package com.p2p.chat.processor;

import org.apache.log4j.Logger;

import com.p2p.chat.Server.ServerUtils;
import com.p2p.chat.container.UserContainer;
import com.p2p.chat.entity.User;
import com.p2p.chat.event.LogoutEvent;

public class LogoutProcessor extends AbstractProcessor {

	static Logger log = Logger.getLogger(LogoutProcessor.class);

	@Override
	protected void doSend() {
		UserContainer.getUC().remove(((LogoutEvent)event).getUser().getIp());
		ServerUtils.getSU().broadcast(event);
	}

	@Override
	protected void doReceive() {
		LogoutEvent event = (LogoutEvent) getEvent();
		User user = event.getUser();
		UserContainer uc = UserContainer.getUC();
		if(uc.contains(user)){
			uc.remove(user.getIp());
		}else{
			log.info("The is not existed��" + user);			
		}
	}

}
