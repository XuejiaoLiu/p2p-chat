//package com.p2p.chat.processor;
//
//import java.io.IOException;
//import org.apache.log4j.Logger;
//
//import com.p2p.chat.Server.ServerUtils;
//import com.p2p.chat.container.EventContainer;
//import com.p2p.chat.event.Event;
//
//public class ReceiveEventProcessor implements Runnable {
//	
//	static Logger log = Logger.getLogger(ReceiveEventProcessor.class);
//
//	ServerUtils su = ServerUtils.getSU();
//	EventContainer ec = EventContainer.getEC();
//	
//	@Override
//	public void run() {
//		while(!Thread.interrupted()){
//			try {
//				Event event = su.receive();
//				ec.push(event);
//			} catch (ClassNotFoundException e) {
//				log.warn("Something wrong",e);
//			} catch (IOException e) {
//				log.warn("Something wrong",e);
//			}
//		}
//	}
//
//}
package com.p2p.chat.processor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import com.p2p.chat.Server.Server;
import com.p2p.chat.container.EventContainer;
import com.p2p.chat.event.Event;

public class ReceiveEventProcessor implements Runnable {
	int send,listen;
	public ReceiveEventProcessor(int send, int listen) throws IOException{
		this.send = send;
		this.listen = listen;
	}
	EventContainer ec = EventContainer.getEC();
	Server server = new Server(send, listen);
	
	@Override
	public void run() {
		while(!Thread.interrupted()){
			//				System.out.println("receive:");
//				String dp = server.receive();
//				System.out.println(dp);
			recieveData();
		}
	}
	
    public void recieveData()
    {
    byte[] dataArray = new byte[1000];
    try
    {
     DatagramSocket ds = new DatagramSocket(listen);
     DatagramPacket dp = new DatagramPacket(dataArray,dataArray.length);
     
     ds.receive(dp); //等待接受从自方8888端口接受来的数据

     ds.close();
     
     System.out.println(new String(dataArray));
     
    } catch (SocketException e)
    {
     e.printStackTrace();
    } catch (IOException e)
    {
     e.printStackTrace();
    }
    }

}

