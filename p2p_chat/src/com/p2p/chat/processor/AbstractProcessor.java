package com.p2p.chat.processor;

import com.p2p.chat.event.Event;


public abstract class AbstractProcessor implements Runnable {


	protected Event event;

//	protected String ip;
//	
//	public String getIp() {
//		return ip;
//	}
//
//	public void setIp(String ip) {
//		this.ip = ip;
//	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
	
	@Override
	public void run() {
		switch(event.getDirection()){
		case CLIENT:
			doReceive();
			break;
		case SERVER:
			doSend();
			break;
		default:
			break;
		}
	}

	protected abstract void doSend();
	protected abstract void doReceive();

}
