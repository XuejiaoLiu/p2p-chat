package com.p2p.chat.processor;

import org.apache.log4j.Logger;

import com.p2p.chat.Server.ServerUtils;
import com.p2p.chat.container.UserContainer;
import com.p2p.chat.entity.User;
import com.p2p.chat.event.LoginEvent;


public class LoginProcessor extends AbstractProcessor {

	static Logger log = Logger.getLogger(LoginProcessor.class);

	public void doSend(){
		ServerUtils.getSU().broadcast(event);
	}

	public void doReceive(){
		User user = ((LoginEvent)event).getUser();
		UserContainer uc = UserContainer.getUC();
		if(!uc.contains(user)){
			uc.add(user.getIp(), user);
			
		}else{
			log.info("The User is existed��" + user);
		}
	}

}
