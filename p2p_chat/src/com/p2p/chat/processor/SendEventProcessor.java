package com.p2p.chat.processor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Scanner;

import com.p2p.chat.Server.Server;
import com.p2p.chat.container.EventContainer;
import com.p2p.chat.event.Event;

public class SendEventProcessor implements Runnable {
	int send,listen;
	public SendEventProcessor(int send, int listen) throws IOException{
		this.send = send;
		this.listen = listen;
	}
	EventContainer ec = EventContainer.getEC();
	Server server = new Server(send, listen);
	
	@Override
	public void run() {
		while(!Thread.interrupted()){
			try {
//				String sendStr = "Hello! I'm Client";
				System.out.println("Type:");
				Scanner in = new Scanner(System.in);
				String sendStr =  in.next();
//				System.out.println("send:"+sendStr);
			    byte[] sendBuf = sendStr.getBytes();
//			    InetAddress addr = InetAddress.getByName("127.0.0.1");
			    InetAddress addr = InetAddress.getLocalHost();
			    DatagramPacket dp = new DatagramPacket(sendBuf ,sendBuf.length,addr,send+1);//8888 -> 8889
				String ip ="127.0.0.1";
				server.send(dp, ip);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
