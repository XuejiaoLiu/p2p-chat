package com.p2p.chat.processor;

import org.apache.log4j.Logger;

import com.p2p.chat.Server.ServerUtils;
import com.p2p.chat.container.EventContainer;

public class ExitProcessor extends AbstractProcessor {
	
	static Logger log = Logger.getLogger(ExitProcessor.class);

	@Override
	protected void doSend() {
		EventContainer.getEC().setLocked(true);
		do{
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				log.warn("Something wrond��", e);
			}
		}while(ServerUtils.getSU().isServerBusy());
		ServerUtils.getSU().closeServer();
	}

	@Override
	protected void doReceive() {
		doSend();
	}

}
