package com.p2p.chat.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.p2p.chat.container.EventContainer;
import com.p2p.chat.event.Event;

import java.util.concurrent.ExecutorService;

import org.apache.log4j.Logger;


public class DispatchEventProcessor implements Runnable {
	
	static Logger log = Logger.getLogger(DispatchEventProcessor.class);


	@SuppressWarnings("rawtypes")
	static Map<Class,AbstractProcessor> map = new HashMap<>();

	ExecutorService exe = Executors.newFixedThreadPool(5);
	
	EventContainer ec = EventContainer.getEC();

	public static void register(Class<? extends Event> clazz,AbstractProcessor run){
		if(!map.containsKey(clazz)){
			map.put(clazz, run);
		}
	}
	public void clearEC(){
		ec.setLocked(true);
		while(!ec.isEmpty()){
			Event event = ec.pop();
			AbstractProcessor pro = map.get(event.getClass());
			pro.setEvent(event);
			exe.execute(pro);
		}
	}
	
	void shutdownAndAwaitTermination(ExecutorService pool) {
		pool.shutdown(); 
		try {
			if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
				pool.shutdownNow(); 
				if (!pool.awaitTermination(60, TimeUnit.SECONDS))
					log.warn("Pool did not terminate");
			}
		} catch (InterruptedException ie) {
			pool.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}

	@Override
	public void run() {
		while(!Thread.interrupted()){
			if(ec.isEmpty()) continue;
			Event event = ec.pop();
			AbstractProcessor pro = map.get(event.getClass());
			pro.setEvent(event);
			exe.execute(pro);
		}
		if(Thread.interrupted()){
			clearEC();
		}
		List<Runnable> list = exe.shutdownNow();
		shutdownAndAwaitTermination(exe);
	}

}
