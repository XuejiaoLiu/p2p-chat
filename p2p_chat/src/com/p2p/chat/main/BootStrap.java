package com.p2p.chat.main;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.p2p.chat.Server.JsonTool;
import com.p2p.chat.container.UserContainer;
import com.p2p.chat.entity.NetworkProperties;
import com.p2p.chat.entity.User;

public class BootStrap {
//	public static ServerSocket serverSocket;
	
	  public static void main(String [] args) throws IOException{
	        if(args[0].equals("--boot"))
	        	initialiseAsBootstrap(args);
	        else if(args[0].equals("--bootstrap"))
	        	initialiseAsJoining(args);
	        else{
	            System.out.println("Please specify --boot <numeric_id> or --bootstrap <ip> <numeric_id>");
	        }
	    }
	    private static void initialiseAsJoining(String [] args){
	        try {
	            int port = NetworkProperties.nPort;
	            NetworkProperties.nAddress = InetAddress.getLocalHost().getHostAddress();  //should give local ip
//	            InetSocketAddress bootstrapAddress = new InetSocketAddress(args[1],port);
//	            DatagramSocket socket = new DatagramSocket(port);
//	            String ip = socket.getLocalSocketAddress().toString();
	            User user1 = new User("gateway",args[1]);
	            UserContainer uc = UserContainer.getUC();
	            uc.add(user1.getIp(), user1);
	        } catch (UnknownHostException e) {
	            e.printStackTrace();
	        }


	    }
	    private static void initialiseAsBootstrap(String [] args) throws IOException {
	        try {
	        	UserContainer userContainer = UserContainer.getUC();
	        	System.out.println("BOOTSTRAP: Waiting message...");
	            int port = NetworkProperties.nPort;
	            NetworkProperties.nAddress = InetAddress.getLocalHost().getHostAddress();  //should give local ip
//	       	 	serverSocket=new ServerSocket(port);
	           
		        DatagramSocket socket = new DatagramSocket(port);
		        byte[] recvBuf = new byte[100];
		        DatagramPacket recvPacket = new DatagramPacket(recvBuf , recvBuf.length);
		        User user1 = new User("gateway","127.0.0.1");
		        UserContainer uc = UserContainer.getUC();
		        uc.add(user1.getIp(), user1);
//	           	 Socket socket1=serverSocket.accept();
		        while(true){
		            socket.receive(recvPacket);
	 	            String recvStr = new String(recvPacket.getData() , 0 , recvPacket.getLength());
	 	            String userinfo[] = recvStr.split(" ");
	 	            int Userlistener = Integer.parseInt(userinfo[2]);
	 	            if(userinfo.length==3){
	 	            	User user = new User(userinfo[0],userinfo[1]);
	 	            	uc.add(user.getIp(), user);
//	 	            	
//	 	            	InetAddress addr = recvPacket.getAddress();
//	 	            	DatagramSocket routing = new DatagramSocket();
//	 	            	
//	 	            	JSONArray routinginfo = null;
//	 	            	JSONObject router = null;
//	 	            	for(User usertemp : userContainer){
//	 	            		JSONObject userjson = new JSONObject();
//	 	            		userjson.put("nickname", usertemp.getNickname());
//	 	            		userjson.put("ip", usertemp.getIp());
//	 	            		routinginfo.add(userjson);
//	 	            		
//	 	            		router.put("talbe", routinginfo);
//	 	            		
//	 	            		JsonTool tool = new JsonTool();
//	 	            		DatagramPacket sendPacket = tool.jsonToDatagram(router, addr,Userlistener);
//	 	            		routing.send(sendPacket);
//			    		}
//	 	            routing.close();
	 	            }
	            }
	            
	        } catch (UnknownHostException e) {
	            e.printStackTrace();
	        }
	    }
	    
}
