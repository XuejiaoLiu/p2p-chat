package com.p2p.chat.main;



import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.p2p.chat.Server.JsonTool;
import com.p2p.chat.entity.Contact;
import com.p2p.chat.entity.MessageType;
import com.p2p.chat.entity.NetworkProperties;

public class Handler {
	  private Map<Integer,Contact> routingTable;
	  private int nodeID;
	  private BlockingQueue sendQueue;
	  private void handleReceivedPacket(DatagramPacket packet){
	        JSONObject jsonObj = new JsonTool().datagramToJson(packet);
	        MessageType messageType;
	    	//well, either further scope the case statements or just expose the variables to the whole switch statement
	    	int sender_id=0, target_id=0, node_id=0,gateway_id=0;
	    	String word = null, ip_address = null;
	    	
	    	//deserialize the whole message. Always put the value as a String and not an Integer even when it is an Integer
	        messageType = MessageType.valueOf((String)jsonObj.get("type"));
	    	sender_id = getAsInteger(jsonObj,"sender_id");
	        target_id = getAsInteger(jsonObj,"target_id");
	        node_id = getAsInteger(jsonObj,"node_id");
	        gateway_id = getAsInteger(jsonObj,"gateway_id");
	        if(jsonObj.containsKey("word")) word = (String)jsonObj.get("word");
	        if(jsonObj.containsKey("ip_address")) ip_address = (String)jsonObj.get("ip_address");
	    		
//	    	Contact closestNode;
	    	JSONObject message;
	    	InetSocketAddress address;
	        try{
	        switch(messageType){
	            case ROUTING_INFO:
	            	singleUpdateRouteTable(sender_id,ip_address);
	            	updateRouteTable((JSONArray)jsonObj.get("route_table"));//dump the all routing_info messages in the routing table
	        		message = jsonObj;
	            	if(gateway_id == nodeID){ // if current is gateway
	                    assert  routingTable.get(node_id).getIp() == "127.0.0.1" : "ip address of node in routing info is wrong";//TODO remember to disable assertions for non-testing environment
	            		address = new InetSocketAddress(routingTable.get(node_id).getIp(),NetworkProperties.nPort);//again ip does not have effect in simulation
	            		sendQueue.put(new JsonTool().jsonToDatagram(message,address));
	            	}else{// pass on to gateway(cuts corners) or TODO implement stateful routing so messsage can go back from hop-to-hop
	            		if(routingTable.containsKey(gateway_id)){
	                        assert  routingTable.get(node_id).getIp() == "127.0.0.1" : "ip address of node in routing info is wrong";
	                		address = new InetSocketAddress(routingTable.get(node_id).getIp(),NetworkProperties.nPort); 
	                		sendQueue.put(new JsonTool().jsonToDatagram(message,address));
	            		}            		
	            	}
	                break;
	                
	            case LEAVING_NETWORK:
	            	routingTable.remove(jsonObj.get("node_id"));
	            	break;
	   
	            case PING:
	            	routingTable.put(sender_id, new Contact(sender_id,ip_address));
	            	singleUpdateRouteTable(sender_id,ip_address);
	            	if(target_id == nodeID){// if current is the suspected dead
	                	message = new JSONObject();
	                	message.put("type", "ACK");
	                	message.put("node_id", String.valueOf(nodeID));
	                	message.put("ip_address",NetworkProperties.nAddress);
	                	address = new InetSocketAddress(packet.getAddress(),packet.getPort());           				
	            		sendQueue.put(new JsonTool().jsonToDatagram(message,address)); 
	            	}else{ // else forward
	            		message = jsonObj;
	            		message.put("ip_address",NetworkProperties.nAddress);//current's address changes each hop
	            		if(routingTable.containsKey(target_id)){
	            			address = new InetSocketAddress(routingTable.get(target_id).getIp(),NetworkProperties.nPort);
	            			sendQueue.put(new JsonTool().jsonToDatagram(message,address));
	            		}else{
	            			
	            		}             	           				           		 
	            	}           	
	                break;

	        }
	        }catch(InterruptedException e){
	          e.printStackTrace();
	        }
	    }
	  private static int getAsInteger(JSONObject jsonObj,String fieldName){
	        int result = 0;
	        if(jsonObj.containsKey(fieldName)){
	            try{//there are some wierd conversion going on when constructing the json
	                result = Integer.parseInt((String)jsonObj.get(fieldName));
	            }catch(NumberFormatException nfe){}
	        }
	        return result;
	    }

	  private void singleUpdateRouteTable(int sender_id, String hostAddress) {
	        if(sender_id == nodeID) return;// current should not be in the table
			long currTime = System.currentTimeMillis();
			if(routingTable.containsKey(sender_id)){
				routingTable.get(sender_id).setTimestamp(currTime);
				routingTable.get(sender_id).setIp(hostAddress);
			}else{
				routingTable.put(sender_id, new Contact(sender_id, hostAddress));
			}	
		}
		private void singleUpdateRouteTable(int sender_id) {
	        if(sender_id == nodeID) return;
			long currTime = System.currentTimeMillis();
			if(routingTable.containsKey(sender_id)){
				routingTable.get(sender_id).setTimestamp(currTime);
			}else{
				routingTable.put(sender_id, new Contact(sender_id, "127.0.0.1"));
			}
		}
	    private void updateRouteTable(JSONArray table){
	    	for(Object row: table){
	    		JSONObject tableNode = (JSONObject) row;
	    		int nodeID = Integer.valueOf((String)tableNode.get("node_id"));
	    		Contact tableContact = new Contact(nodeID, (String)tableNode.get("ip_addresss"));
	    		tableContact.setTimestamp(System.currentTimeMillis());
	    		routingTable.put(nodeID,tableContact);
	    	}
	    }
}
