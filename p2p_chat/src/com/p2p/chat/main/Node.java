package com.p2p.chat.main;

import java.io.IOException;
import java.io.PrintStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;








//import com.p2p.chat.container.ClientContainer;
import com.p2p.chat.container.EventContainer;
import com.p2p.chat.container.UserContainer;
import com.p2p.chat.entity.NetworkProperties;
import com.p2p.chat.entity.User;
import com.p2p.chat.event.EventBuilder;
import com.p2p.chat.event.ExitEvent;
import com.p2p.chat.event.LoginEvent;
import com.p2p.chat.event.LogoutEvent;
import com.p2p.chat.event.MessageEvent;
import com.p2p.chat.processor.DispatchEventProcessor;
import com.p2p.chat.processor.ExitProcessor;
import com.p2p.chat.processor.LoginProcessor;
import com.p2p.chat.processor.LogoutProcessor;
import com.p2p.chat.processor.MessageProcessor;
import com.p2p.chat.processor.ReceiveEventProcessor;

import java.util.concurrent.ExecutorService;


public class Node {
	
	/**

	{
	    "type": "JOINING_NETWORK_SIMPLIFIED", // a string
	    "node_id": "42", // a non-negative number of order 2'^32^', indicating the id of the joining node).
	    "target_id": "42", // a non-negative number of order 2'^32^', indicating the target node for this message.
	    "ip_address": "199.1.5.2" // the ip address of the joining node
	}
	{
	    "type": "JOINING_NETWORK_RELAY_SIMPLIFIED", // a string
	    "node_id": "42", // a non-negative number of order 2'^32^', indicating the id of the joining node).
	    "target_id": "42", // a non-negative number of order 2'^32^', indicating the target node for this message.
	    "gateway_id": "34", // a non-negative number of order 2'^32^', of the gateway node
	}

	{
	    "type": "ROUTING_INFO", // a string
	    "gateway_id": "34", // a non-negative number of order 2'^32^', of the gateway node
	    "node_id": "42", // a non-negative number of order 2'^32^', indicating the target node (and also the id of the joining node).
	    "ip_address": "199.1.5.2" // the ip address of the node sending the routing information
	    "route_table":
	    [
	        {
	            "node_id": "3", // a non-negative number of order 2'^32^'.
	            "ip_address": "199.1.5.3" // the ip address of node 3
	        },
	        {
	            "node_id": "22", // a non-negative number of order 2'^32^'.
	            "ip_address": "199.1.5.4" // the ip address of  node 22
	        }
	    ]
	}

	{
	    "type": "LEAVING_NETWORK", // a string
	    "node_id": "42", // a non-negative number of order 2'^32^' identifying the leaving node.
	}

	{
	    "type": "INDEX", //string
	    "target_id": "34", //the target id
	    "sender_id": "34", // a non-negative number of order 2'^32^', of the message originator
	    "keyword": "XXX", //the word being indexed
	    "link": [
	               "http://www.newindex.com", // the url the word is found in
	               "http://www.xyz.com"
	              ]
	}

	{
	    type: "CHAT", //string
	    target_id: "42", //the target id
	    sender_id: "34", // a non-negative number of order 2'^32^', of the message originator
	    tag: "Abba", //the tag being indexed
	    text: "I love #Abba" // the text of the chat
	}

	ACK_CHAT
	{
	    type: "ACK_CHAT", // a string
	    node_id: "23", // a non-negative number of order 2'^32^', identifying the target node.
	    tag: "fish" // the keyword from the original CHAT message
	}
	CHAT_RETRIEVE
	{
	    type: "CHAT_RETRIEVE", // string
	    tag: "Abba", // The word to search for
	    node_id: "34",  // target node id
	    sender_id: "42", // a non-negative number of order 2'^32^', of this message originator
	}
	CHAT_RESPONSE
	{
	    type: "CHAT_RESPONSE",
	    tag: "Abba", // The tag searched for
	    node_id: "42",  // target node id
	    sender_id: "34", // a non-negative number of order 2'^32^', of this message originator
	    response:
	    [
	        {
	            text: "I love #Abba"
	        },
	        {
	             text: "I think #Abba are great too"w
	        }
	    ]

	}
	{
	    "type": "SEARCH_RESPONSE",
	    "word": "word", // The word to search for
	    "node_id": "45",  // target node id
	    "sender_id": "34", // a non-negative number of order 2'^32^', of this message originator
	    "response":
	    [
	        {
	            url: "www.dsg.cs.tcd.ie/",  //url
	            rank: "32"  //rank
	        },
	        {
	             url: "www.scss.tcd.ie/courses/mscnds/",  //url
	             rank: "1" //rank
	        }
	    ]
	}

	{
	    "type": "PING", // a string
	    "target_id": "23", // a non-negative number of order 2'^32^', identifying the suspected dead node.
	    "sender_id": "56", // a non-negative number of order 2'^32^', identifying the originator 
	                               //    of the ping (does not change)
	   "ip_address": "199.1.5.4" // the ip address of  node sending the message (changes each hop)
	}

	{
	    "type": "ACK", // a string
	    "node_id": "23", // a non-negative number of order 2'^32^', identifying the suspected dead node.
	    "ip_address": "199.1.5.4" // the ip address of  sending node, this changes on each hop (or used to hold the keyword in an ACK message returned following an INDEX message - see note below)
	}

	{
	    "type": "ACK_INDEX", // a string
	    "node_id": "23", // a non-negative number of order 2'^32^', identifying the target node.
	    "keyword": "fish" // the keyword from the original INDEX message 
	}
	 */
	
	
	
	static enum Menu{
		LOGIN("Login"),
		LOGOUT("Logout"),
		SHOW_ALL_USER("User List"),
		MESSAGE("Message to"),
		EXIT("Exit Chat");
		
		String description;

		public String getDescription() {
			return description;
		}

		private Menu(String description) {
			this.description = description;
		}
		
	}
	
	ExecutorService exe = Executors.newFixedThreadPool(2);

	EventContainer eventContainer = EventContainer.getEC();

	UserContainer userContainer = UserContainer.getUC();

	PrintStream out = System.out;
	
	Scanner in = new Scanner(System.in);
	
	boolean isLogin = false;
	
	User currentUser = null;

	boolean isRunning = false;
	
	int send,listen;
	
	public void init(int send, int listen) throws IOException{
		exe.execute(new DispatchEventProcessor());
		exe.execute(new ReceiveEventProcessor(send,listen));
	}

	public void displayMenu(){
		out.println(">>>>>>>>>>>>>>>>Menu<<<<<<<<<<<<<<<<");
		for(Menu menu : Menu.values()){
			out.println((menu.ordinal() + 1) + ", " + menu.getDescription());
		}
		out.println(">>>>>>>>>>>>>>>>****<<<<<<<<<<<<<<<<");
		out.print("Type your order:");
	}

	public void login(int send, int listen) throws IOException{
		if(isLogin){
			out.println("You have logged in.");
			return;
		}
		User user = null;
		System.out.print("Please Type Your Nickname：");
		String nickname = new Scanner(System.in).next();
		String ip = null;
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		user = new User(nickname, ip);
		String userinfo = user.getNickname()+" "+user.getIp()+" "+listen;
		byte [] sendBuf = userinfo.getBytes();
		DatagramSocket client = new DatagramSocket(send);
		InetAddress addr = InetAddress.getByName("127.0.0.1");
	    int port = NetworkProperties.nPort;
	    DatagramPacket sendPacket = new DatagramPacket(sendBuf ,sendBuf.length , addr , port);
	    client.send(sendPacket);
	    client.close();
//	    reRouting(listen);
	    
		LoginEvent event = EventBuilder.buildLoginEvent(user);
		eventContainer.push(event);
		this.currentUser = user;
		this.isLogin = true;
	}
	
	private static void reRouting(int listen) throws IOException {
		// TODO Auto-generated method stub
		  DatagramSocket socket = new DatagramSocket(listen);
	      byte[] recvBuf = new byte[100];
	      DatagramPacket recvPacket = new DatagramPacket(recvBuf , recvBuf.length);
		
	      while(true){
	    	  socket.receive(recvPacket);
	    	  String recvStr = new String(recvPacket.getData() , 0 , recvPacket.getLength());
	    	  System.out.println(recvStr);
	      }
	}
	
	public void showAllUser(){
		out.println((this.currentUser != null ? this.currentUser.getNickname() : "Please Login firstly！"));
		for(User user : userContainer){
			out.println(user);
		}
	}
	
	public void message(){
		out.println("Please Type Receiver Nickname：");
		String nickname = in.next();
		User user = null;
		String ip = null;
		for(User usertemp : userContainer){
			if(usertemp.getNickname().equals(nickname)){
				ip = usertemp.getIp();
			}
		}
		user = userContainer.get(ip);
		if(userContainer.contains(user)){
			MessageEvent event = EventBuilder.bulidMsgEvent(user);
			eventContainer.push(event);
		}
	}
	public void exit(){
		out.println("Exit？(Y/N)");
		String input = in.next();
		if(input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes")){
			if(isLogin){
				logout();
			}
			ExitEvent event = EventBuilder.buildExitEvent();
			eventContainer.push(event);
			exe.shutdownNow();
			isRunning = false;
			try {
				exe.awaitTermination(60, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	

	public int readMenu(){
		int menu = 0;
		while(true){
			if(in.hasNextInt()){
				menu = in.nextInt();
				if(menu >= 1 && menu <= Menu.values().length){
					break;
				}
			}
			out.println("Wrong Order, Please Retype：");
			in.next();
		}
		return menu - 1;
	}

	public void run() throws IOException{
		isRunning = true;
		while(isRunning){
			displayMenu();
			int menuOrder = readMenu();
			switch(Menu.values()[menuOrder]){
			case LOGIN:
				login(send,listen);
				break;
			case SHOW_ALL_USER:
				showAllUser();
				break;
			case LOGOUT:
				logout();
				break;
			case EXIT:
				exit();
				break;
			case MESSAGE:
				message();
				break;
			default:
			}
			
		}
	}

	private void logout() {
		if(isLogin){
			LogoutEvent event = EventBuilder.buildLogoutEvent(currentUser);
			eventContainer.push(event);
			out.println("Logout!");
		}else{
			out.println("Please Login Firstly!");
		}
		isLogin = false;
		this.currentUser = null;
	}
	public static void main(String[] args) throws IOException{
		//注册事件类型和相应的执行器
		int send=Integer.parseInt(args[0]);
		int listen=Integer.parseInt(args[1]);
		
		DispatchEventProcessor.register(LoginEvent.class, new LoginProcessor());
		DispatchEventProcessor.register(LogoutEvent.class, new LogoutProcessor());
		DispatchEventProcessor.register(ExitEvent.class, new ExitProcessor());
		DispatchEventProcessor.register(MessageEvent.class, new MessageProcessor());
		
		Node conImpl = new Node();
		conImpl.init(send,listen);
		conImpl.run();
	}
}
