package com.p2p.chat.main;

import java.io.PrintStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;



//import com.p2p.chat.container.ClientContainer;
import com.p2p.chat.container.EventContainer;
import com.p2p.chat.container.UserContainer;
import com.p2p.chat.entity.User;
import com.p2p.chat.event.EventBuilder;
import com.p2p.chat.event.ExitEvent;
import com.p2p.chat.event.LoginEvent;
import com.p2p.chat.event.LogoutEvent;
import com.p2p.chat.event.MessageEvent;
import com.p2p.chat.processor.DispatchEventProcessor;
import com.p2p.chat.processor.ExitProcessor;
import com.p2p.chat.processor.LoginProcessor;
import com.p2p.chat.processor.LogoutProcessor;
import com.p2p.chat.processor.MessageProcessor;
import com.p2p.chat.processor.ReceiveEventProcessor;

import java.util.concurrent.ExecutorService;

/**
 * 显示客户端，黑窗口版
 * @author Administrator
 *
 */
public class P2PChat {
	
	static enum Menu{
		LOGIN("Login"),
		LOGOUT("Logout"),
		SHOW_ALL_USER("User List"),
		MESSAGE("Message to"),
		EXIT("Exit Chat");
		
		String description;

		public String getDescription() {
			return description;
		}

		private Menu(String description) {
			this.description = description;
		}
		
	}
	
	ExecutorService exe = Executors.newFixedThreadPool(2);

	EventContainer eventContainer = EventContainer.getEC();

	UserContainer userContainer = UserContainer.getUC();

	PrintStream out = System.out;
	
	Scanner in = new Scanner(System.in);
	
	boolean isLogin = false;
	
	User currentUser = null;

	boolean isRunning = false;
	
	public void init(){
		exe.execute(new DispatchEventProcessor());
		exe.execute(new ReceiveEventProcessor());
	}

	public void displayMenu(){
		out.println(">>>>>>>>>>>>>>>>Menu<<<<<<<<<<<<<<<<");
		for(Menu menu : Menu.values()){
			out.println((menu.ordinal() + 1) + ", " + menu.getDescription());
		}
		out.println(">>>>>>>>>>>>>>>>****<<<<<<<<<<<<<<<<");
		out.print("Type your order:");
	}

	public void login(){
		if(isLogin){
			out.println("You have logged in.");
			return;
		}
		User user = null;
		out.print("Please Type Your Nickname：");
		String nickname = in.next();
		String ip = null;
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		user = new User(nickname, ip);
		LoginEvent event = EventBuilder.buildLoginEvent(user);
		eventContainer.push(event);
		this.currentUser = user;
		this.isLogin = true;
	}
	
	public void showAllUser(){
		out.println((this.currentUser != null ? this.currentUser.getNickname() : "Please Login firstly！"));
		for(User user : userContainer){
			out.println(user);
		}
	}
	
	public void message(){
		out.println("Please Type Receiver Nickname：");
		String nickname = in.next();
		User user = null;
		String ip = null;
		for(User usertemp : userContainer){
			if(usertemp.getNickname().equals(nickname)){
				ip = usertemp.getIp();
			}
		}
		user = userContainer.get(ip);
		if(userContainer.contains(user)){
			MessageEvent event = EventBuilder.bulidMsgEvent(user);
			eventContainer.push(event);
		}
	}
	public void exit(){
		out.println("Exit？(Y/N)");
		String input = in.next();
		if(input.equalsIgnoreCase("y") || input.equalsIgnoreCase("yes")){
			if(isLogin){
				logout();
			}
			ExitEvent event = EventBuilder.buildExitEvent();
			eventContainer.push(event);
			exe.shutdownNow();
			isRunning = false;
			try {
				exe.awaitTermination(60, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	

	public int readMenu(){
		int menu = 0;
		while(true){
			if(in.hasNextInt()){
				menu = in.nextInt();
				if(menu >= 1 && menu <= Menu.values().length){
					break;
				}
			}
			out.println("Wrong Order, Please Retype：");
			in.next();
		}
		return menu - 1;
	}

	public void run(){
		isRunning = true;
		while(isRunning){
			displayMenu();
			int menuOrder = readMenu();
			switch(Menu.values()[menuOrder]){
			case LOGIN:
				login();
				break;
			case SHOW_ALL_USER:
				showAllUser();
				break;
			case LOGOUT:
				logout();
				break;
			case EXIT:
				exit();
				break;
			case MESSAGE:
				message();
				break;
			default:
			}
			
		}
	}

	private void logout() {
		if(isLogin){
			LogoutEvent event = EventBuilder.buildLogoutEvent(currentUser);
			eventContainer.push(event);
			out.println("Logout!");
		}else{
			out.println("Please Login Firstly!");
		}
		isLogin = false;
		this.currentUser = null;
	}
	public static void main(String[] args){
		//注册事件类型和相应的执行器
		DispatchEventProcessor.register(LoginEvent.class, new LoginProcessor());
		DispatchEventProcessor.register(LogoutEvent.class, new LogoutProcessor());
		DispatchEventProcessor.register(ExitEvent.class, new ExitProcessor());
		DispatchEventProcessor.register(MessageEvent.class, new MessageProcessor());
		
		P2PChat conImpl = new P2PChat();
		conImpl.init();
		conImpl.run();
	}
}
