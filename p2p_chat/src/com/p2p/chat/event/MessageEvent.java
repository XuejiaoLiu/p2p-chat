package com.p2p.chat.event;

import com.p2p.chat.entity.User;

public class MessageEvent extends Event{

	private static final long serialVersionUID = 2134948861263987345L;
	User user;

	public User getUser() {
		return user;
	}

	public MessageEvent(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Message Event [user=" + user + ", direction=" + direction + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

}
