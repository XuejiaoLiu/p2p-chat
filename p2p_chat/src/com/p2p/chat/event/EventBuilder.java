package com.p2p.chat.event;

import com.p2p.chat.entity.User;
import com.p2p.chat.event.Event.Direction;

public class EventBuilder {
	
	public static LoginEvent buildLoginEvent(User user){
		LoginEvent event = null;
		event = new LoginEvent(user);
		event.setDirection(Direction.SERVER);
		return event;
	}

	public static LogoutEvent buildLogoutEvent(User user){
		LogoutEvent event = null;
		event = new LogoutEvent(user);
		event.setDirection(Direction.SERVER);
		return event;
	}


	public static ExitEvent buildExitEvent(){
		ExitEvent event = new ExitEvent();
		event.setDirection(Direction.SERVER);
		return event;
	}
	
	public static MessageEvent bulidMsgEvent(User user){
		MessageEvent event = new MessageEvent(user);
		event.setDirection(Direction.SERVER);
		return event;
	}
}
