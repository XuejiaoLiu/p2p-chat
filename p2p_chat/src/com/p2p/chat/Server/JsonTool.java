package com.p2p.chat.Server;

import java.io.IOException;
import java.io.StringWriter;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JsonTool {
	 private JSONObject ArraytoJSON(JSONArray array){
		 JSONObject obj = null;
		return obj;
		 
	 }

	 public DatagramPacket jsonToDatagram(JSONObject json,InetSocketAddress sendAddress){
	        StringWriter out = new StringWriter();
	        DatagramPacket sendPacket = null;  // blahh,
	        try {                                  // non-business logic
	        	byte [] sendBuffer;                   // who cares? :)
	        	if(json == null){
	        		sendBuffer = new byte[1];
	        	}else{
	        		json.writeJSONString(out);
	        		sendBuffer = out.toString().getBytes();
	        	}
	            sendPacket = new DatagramPacket(sendBuffer,sendBuffer.length,sendAddress);
	        } catch (SocketException e) {
	            System.err.println("Could put address on packet in Controller");
	            e.printStackTrace();
	        }catch (IOException e) {
	            System.err.println("Could not convert JSON to String in Controller");
	            e.printStackTrace();
	        }
	        return sendPacket;
	    }
	    public JSONObject datagramToJson(DatagramPacket packet){
	        Object jsonObj = null;
	        try {
	            String receivedData = new String(packet.getData());
	            int i=0;//the json does not include the length so we have get it manually
	            while(i < receivedData.length() && isValidChar(receivedData.charAt(i)))i++;
	            receivedData = receivedData.substring(0,i);
//	            System.out.println("received: "+receivedData.toString());
	            JSONParser parser = new JSONParser();
	            jsonObj = parser.parse(receivedData);
	        } catch (ParseException e) {
	            System.err.println("Could not parse incoming message in Controller");
	            e.printStackTrace();
	        }
	        return (JSONObject)jsonObj;
	    }
	    private boolean isValidChar(char c) {
	        return ((int) c >= 32 && (int)c <=126);
	    }
}
