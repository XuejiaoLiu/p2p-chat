package com.p2p.chat.Server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

import org.apache.log4j.Logger;

public class Server {
	
	DatagramSocket sender;
	DatagramSocket listener;
	boolean busy = false;
	public boolean isBusy() {
		return busy;
	}
	public synchronized void setBusy(boolean busy) {
		this.busy = busy;
	}
	public Server(int sendport,int listenport) throws IOException{
	
		
		sender = new DatagramSocket(sendport,InetAddress.getLocalHost());
		listener = new DatagramSocket(listenport,InetAddress.getLocalHost());
		
	}
	public synchronized void broadcast(DatagramPacket dp) throws IOException{
		dp.setSocketAddress(new InetSocketAddress("255.255.255.255", ServerUtils.listenport));
		sender.setBroadcast(true);
		setBusy(true);
		sender.send(dp);
		setBusy(false);
		sender.setBroadcast(false);
	}
	public synchronized void send(DatagramPacket dp,String ip) throws IOException{
		dp.setSocketAddress(new InetSocketAddress(ip, ServerUtils.listenport));
		setBusy(true);
		sender.send(dp);
		setBusy(false);
	}
	public DatagramPacket receive() throws IOException{
		byte[] buffer = new byte[1024 * 4];
		DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
		listener.receive(dp);
		return dp;
	}
	public void close(){
		sender.close();
		listener.close();
	}

}
