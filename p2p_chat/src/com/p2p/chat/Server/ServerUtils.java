package com.p2p.chat.Server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.p2p.chat.event.Event;

public class ServerUtils {

	public static final Integer sendport = 8888;
	
	public static final Integer listenport = 8889;
	
//	static Logger log = Logger.getLogger(ServerUtils.class);
	
	private Server server;

	private static ServerUtils serverUtils = new ServerUtils();
	
	private ServerUtils(){
		try {
			this.server = new Server(sendport,listenport);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static ServerUtils getSU(){
		return serverUtils;
	}
	public void broadcast(Event event){
		server.setBusy(true);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			event.setDirection(Event.Direction.CLIENT);
			oos.writeObject(event);
			byte[] buffer = bos.toByteArray();
			server.broadcast(new DatagramPacket(buffer, buffer.length));
		} catch (IOException e) {
			e.printStackTrace();
		}
		server.setBusy(false);
	}
	
	public void send(Event event,String ip){
		server.setBusy(true);
		event.setDirection(Event.Direction.CLIENT);
		try {
			System.out.println("Type your message:");
			
			Scanner in = new Scanner(System.in);
			String message = in.next();
//			ByteArrayOutputStream bos = new ByteArrayOutputStream();
//			ObjectOutputStream oos = new ObjectOutputStream(bos);
//			oos.writeObject(event);
//			byte[] buffer = bos.toByteArray();
			byte[] buffer = message.getBytes();
//			log.debug("发送数据成功写入缓存：" + event);
			DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
			
			
			System.out.println("Server IP address: "+ip );
			server.send(dp,ip);
		} catch (IOException e) {
//			log.warn("发送数据失败", e);
			e.printStackTrace();
		}
		server.setBusy(false);
//		log.info("发送数据成功：" + event);
	}

	public Event receive() throws IOException, ClassNotFoundException{
		DatagramPacket dp = null;
		dp = server.receive();
		if(dp == null) return null;
		
		ByteArrayInputStream bis = new ByteArrayInputStream(dp.getData());
		ObjectInputStream ois = new ObjectInputStream(bis);
		Object o = ois.readObject();
		return (Event) o;
	}
	
	public void closeServer(){
		this.server.close();
	}
	
	public boolean isServerBusy(){
		return this.server.isBusy();
	}
}
