package com.p2p.chat.container;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;

import com.p2p.chat.event.Event;


public class EventContainer {
	
	static Logger log = Logger.getLogger(EventContainer.class);
	

	private static EventContainer eventContainer = new EventContainer();
	
	private EventContainer(){
	}
	
	public static EventContainer getEC(){
		return eventContainer;
	}

	Queue<Event> eventQueue = new ConcurrentLinkedQueue<>();
	
	boolean isLocked = false;
	
	public synchronized void push(Event event){
		if(event != null && !isLocked()){
			eventQueue.add(event);
		}else{
			log.info("The Queue is Locked!");
		}
	}

	public Event pop(){
		return eventQueue.poll();
	}
	public boolean isEmpty(){
		return eventQueue.isEmpty();
	}
	public void clear(){
		eventQueue.clear();
	}

	public boolean isLocked() {
		return isLocked;
	}

	public synchronized void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}
}
