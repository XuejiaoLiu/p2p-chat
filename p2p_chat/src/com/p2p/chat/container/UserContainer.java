package com.p2p.chat.container;


import java.util.Iterator;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.p2p.chat.entity.User;
public class UserContainer implements Iterable<User> {
	
	private static UserContainer uc = new UserContainer();
	
	private UserContainer(){
		
	}
	
	public static UserContainer getUC(){
		return uc;
	}
	Map<String,User> map = new ConcurrentHashMap<>();
	
	public User get(String key){
		return map.get(key);
	}
	@Override
	public Iterator<User> iterator() {
		return map.values().iterator();
	}
	
	public void add(String ip,User user){
		if(!map.containsValue(user)){
			map.put(ip, user);
			System.out.println("Add New User:"+user.getNickname()+" ["+user.getIp()+"]");
		}
	}
	
	public void remove(String ip){
		map.remove(ip);
	}
	
	public boolean contains(User user){
		return map.containsValue(user);
	}
}
